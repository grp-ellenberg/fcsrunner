# ![fcsrunner_icon.png](./resources/fcsrunner_icon.png) FcsRunner 

> **Supplementary software 1** of 
>>  Quantitative mapping of fluorescently tagged cellular proteins using FCS-calibrated four dimensional imaging
Antonio Z Politi, Yin Cai, Nike Walther, M. Julius Hossain, Birgit Koch, Malte Wachsmuth, Jan Ellenberg (2018)
bioRxiv 188862; doi: https://doi.org/10.1101/188862.
> 
> Please cite the above work when using this tool.

VBA tool to perform imaging and fluorescence (cross)correlation spectroscopy  (F(C)CS) measurements and automatically save FCS positions to a xml file. 
To be used with Zeiss LSM microscopes running with ZEN black (version >= 2010). 
The tool can be used to create data for FCS-calibrated imaging. 

FCS measurements are performed at multiple stage positions, typically one stage position per cell. 
At each stage position the user specifies FCS points. After all positions have been specified the users starts the acquisition. 
Then for each position an image is acquired and FCS measurements are performed. The image and FCS settings are given by the current settings in ZEN. 

For further details refer to the [Wiki](https://git.embl.de/politi/fcsrunner/wikis/home).

# Author
Antonio Politi, EMBL, Ellenberg group