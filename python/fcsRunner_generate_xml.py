# -*- coding: utf-8 -*-
"""
Created on Mon Aug 03 12:20:09 2015
Generate an xml version compatible with fcsImageBrowser.
From version of FCSRunner > 0.5 this script has become unnecessary.
Object ID describes cell for a given set. In manual acquisition ID can be higher than 1
@author: Antonio Politi, EMBL
"""
import os
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom
from xml.etree import ElementTree
from glob import glob
import Tkinter, Tkconstants, tkFileDialog


def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def modifyXmlFiles(rootdir, cls):
    '''
    add or changes entries of xml file describing the position of acquired points
    '''
    fcsFiles = glob(os.path.join(rootdir, '*.fcs'))
    for fcsFile  in fcsFiles:
        dirname = os.path.split(fcsFile)[0]
        fcsname = os.path.splitext(os.path.basename(fcsFile))[0]

        tree = ElementTree.parse(os.path.join(dirname, fcsname +'.xml'))
        root = tree.getroot()
        
        for idx, child in enumerate(root.iter('object')):
            child.attrib = {'ID': '1'}
            if (idx+1) in cls[0][0]:
                child[0].text = cls[0][1] 
            else:
                child[0].text = cls[1][1]
        tree.write(os.path.join(dirname, fcsname +'.xml'), encoding='utf-8', xml_declaration=True)
        
def makeXmlFiles(rootdir, classes, zpos=0, LSM = 0):
    '''
    make an xml file for manual acquired data
    :param rootdir:
    :param classes: a list [[[1, 2, 5, 6, 9, 10, 13, 14],'nuc'], [[3, 4, 7, 8, 11, 12, 15, 16], 'cyt']]
    :param zpos:
    :param LSM:
    '''
    fcsFiles = glob(os.path.join(rootdir, '*.fcs'))
    txtFiles = glob(os.path.join(rootdir, '*.txt'))
    lsmFiles = glob(os.path.join(rootdir, '*.lsm'))
    
    fcsImgPos = []
    for fcsFile in fcsFiles:
        dirname = os.path.split(fcsFile)[0]
        fcsname = os.path.splitext(os.path.basename(fcsFile))[0]
        txtFile = os.path.join(dirname, fcsname +'.txt')
        #image can be a pre or postFCS image
        lsmFile =  os.path.join(dirname, fcsname +'_preFCS.lsm')
        lsmFile2 = os.path.join(dirname, fcsname +'_postFCS.lsm')
        if os.path.exists(txtFile):
            if os.path.exists(lsmFile):
                fcsImgPos.append([fcsFile, lsmFile, txtFile])
            else:
                if os.path.exists(lsmFile2):
                    fcsImgPos.append([fcsFile, lsmFile2, txtFile])
                else:
                    fcsImgPos.append([fcsFile, lsmFiles[0], txtFile])
    
    for fcsSet in fcsImgPos:
        xmlFile = open(os.path.splitext(fcsSet[0])[0] + '.xml', 'w')
        root = Element('xml')
        img = SubElement(root, 'Image', {'Name': fcsSet[1]})
        txtfile = open(fcsSet[2])
        ID = 0
        changeID = True
        idx  = 0
        for line in txtfile:
            if line[0] == '%': 
                continue
            idx = idx+1
            if idx < 7 and LSM:
                continue
            arr =  line.rstrip('\n').split()
            print(arr)
            try:
                pos = [float(a) for a in arr]
                obj = SubElement(root, 'object')
                cls = SubElement(obj, 'class')
                print(classes[0][0])
                if any([idx == e for e in classes[0][0]]):
                    print idx
                    cls.text = classes[0][1]
                    if changeID:
                        ID = ID + 1
                        changeID = False
                else:
                    cls.text = classes[1][1]
                    changeID = True

                obj.attrib = {'ID': str(ID)}
                x = SubElement(obj, 'x')
                y = SubElement(obj, 'y')

                if len(pos) < 4:
                    x.text = str(int(pos[0]/scaling + (imgSize-1.0)/2.0))
                    y.text = str(int(pos[1]/scaling + (imgSize-1.0)/2.0))
                else:
                    x.text = str(int(pos[3] + (imgSize-1.0)/2.0))
                    y.text = str(int(pos[4] + (imgSize-1.0)/2.0))
                    #x.text = str(int(pos[0]/scaling + (imgSize-1.0)/2.0))
                    #y.text = str(int(pos[1]/scaling + (imgSize-1.0)/2.0))
                    #sometimes this helps
                    #x.text = str(int(pos[3] + (imgSize-1.0)/2.0))
                    #y.text = str(int(pos[4] + (imgSize-1.0)/2.0))
                z = SubElement(obj, 'z')
                z.text = str(zpos)

            except ValueError:
                # this catches if first value is not a float
                continue

            
        xmlFile.write(prettify(root))
        xmlFile.close()



def classesID(nrEl, names):
    ''' returns index of class with names. nrEl contains number of element of each class'''
    nrCells = range(0,10)
    elArr = [1]*len(nrEl)
    for i in range(0, len(nrEl)):    
        if i == 0:
            elArr[i] = [[el for cell in nrCells for el in range(cell*sum(nrEl) + 1, cell*sum(nrEl)+ nrEl[i] + 1)], names[i]]
        else:
            elArr[i] = [[el for cell in nrCells for el in range(cell*sum(nrEl) + nrEl[i-1] + 1,cell*sum(nrEl) + sum(nrEl[0:i+1]) + 1)], names[i]]
    return elArr
    
def getLSMdirectories():
    ''' descent tree up to level 1 and get diretories named LSM''' 
    res = []
    for root, dirs, files in os.walk(rootdir, topdown=True):
        
        depth = root[len(rootdir) + len(os.path.sep):].count(os.path.sep)
        if depth == 0:
            bname = os.path.basename(root)
            try:
                if(int(bname[0:2]) > 15):
                    continue
            except ValueError:
                continue
        if depth == 1:
            bname = os.path.basename(os.path.dirname(root))
            try:
                if(int(bname[0:2]) > 15):
                    dirs[:] = [] # Don't recurse any deeper
                    continue
            except ValueError:
                dirs[:] = [] # Don't recurse any deeper
                continue
            # We're currently two directories in, so all subdirs have depth 3
            res += [os.path.join(root, d) for d in dirs if d == 'LSM']
            dirs[:] = [] # Don't recurse any deeper
    return(res)  
   

def getGFPdirectories():
    ''' descent tree up to level 1 and get diretories named LSM'''
    res = []
    for root, dirs, files in os.walk(rootdir, topdown=True):
        
        depth = root[len(rootdir) + len(os.path.sep):].count(os.path.sep)
        if depth == 0:
            bname = os.path.basename(root)
            try:
                if(int(bname[0:2]) > 15):
                    continue
            except ValueError:
                continue

        if depth == 1:
            bname = os.path.basename(os.path.dirname(root))
            try:
                if(int(bname[0:2]) > 15):
                    dirs[:] = [] # Don't recurse any deeper
                    continue
            except ValueError:
                dirs[:] = [] # Don't recurse any deeper
                continue
            # We're currently two directories in, so all subdirs have depth 3
            res += [os.path.join(root, d) for d in dirs if d == 'Calibration']
            dirs[:] = [] # Don't recurse any deeper
    return(res)  

def getRootdirectories(rootdir):
    ''' descent tree up to level 1 and get diretories named LSM'''
    res = []
    for root, dirs, files in os.walk(rootdir, topdown=True):
        dirs.sort()
        depth = root[len(rootdir) + len(os.path.sep):].count(os.path.sep)
        if depth == 0:
            bname = os.path.basename(root)
            try:
                if(int(bname[0:2]) > 18):
                    continue
            except ValueError:
                continue
                        # We're currently two directories in, so all subdirs have depth 3
            res += [os.path.join(root, d) for d in dirs if  ('MitoSys' in d) or ('Mitosys' in d)]
            
        if depth == 1:
            bname = os.path.basename(os.path.dirname(root))
            try:
                if(int(bname[0:2]) > 18):
                    dirs[:] = [] # Don't recurse any deeper
                    continue
            except ValueError:
                dirs[:] = [] # Don't recurse any deeper
                continue
            # We're currently two directories in, so all subdirs have depth 3
            #res += [os.path.join(root, d) for d in dirs if d == 'Calibration']
            dirs[:] = [] # Don't recurse any deeper
    return(res)


Tkinter.Tk().withdraw()
opt = {}
opt['initialdir'] = r'T:\Antonio (apoliti)\EMBL\Cohesin'

indir = tkFileDialog.askdirectory(**opt)
imgSize = 512
scaling = 0.2076
# 3 nuc and 2 cyt
classes_3_2 = [[[i + 5*j for j in range(0, 4) for i in range(1,4)],'nuc'], [[i + 5*j for j in range(0, 4) for i in range(4,6)], 'cyt']]
# 3 nuc and 3 cyt
classes_3_3 = [[[i + 6*j for j in range(0, 4) for i in range(1,4)],'nuc'], [[i + 6*j for j in range(0, 4) for i in range(4,7)], 'cyt']]

makeXmlFiles(indir, classes_3_2)
'''

rootdir = r'V:\MitoSys_data\Data'
res = getRootdirectories(rootdir)

print(res)
fid = open(os.path.join(rootdir, 'calibration_file_list.txt'), 'w')
fid.write('\"path\"\t\"res\"\t\"fint\"\t\"fint2dairy\"\t\"fint3dairy\"\t\"cell\"\t\"maindir\"\n')
for i in range(0, len(res)):
    fid.write('\"%s\"\t\"%s\"\tNA\tNA\tNA\t\"dye\"\t\"V:/MitoSys_data/Data/\"\n' % (path[i], focvol[i]))
    fid.write('\"%s\"\t\"%s\"\tNA\tNA\tNA\t\"all\"\t\"V:/MitoSys_data/Data/\"\n' % (path[i], resfile[i]))
    
fid.close()


# remove first list of it 
res  = [re.sub('\\\\', '/', r) for r in res]
path = [re.sub('V:/MitoSys_data/Data/', './', r) for r in res]
resfile =  [p + '/2c_opt.res' for p in path]

focvol =  [p + '/Calibration/Alexa/focalVolume.txt' if  int(p[2:4]) <= 15 else '/Calibration/dye/focalVolume.txt' 
for p in path]

#idx_new = [int(r[2:4]) > 15 for r in path]
#focvol[idx_new] = focvolnew[idx_new]


fid = open(os.path.join(rootdir, 'calibration_file_list.txt'), 'w')
fid.write('\"path\"\t\"res\"\t\"fint\"\t\"fint2dairy\"\t\"fint3dairy\"\t\"cell\"\t\"maindir\"\n')
for i in range(0, len(res)):
    fid.write('\"%s\"\t\"%s\"\tNA\tNA\tNA\t\"dye\"\t\"V:/MitoSys_data/Data/\"\n' % (path[i], focvol[i]))
    fid.write('\"%s\"\t\"%s\"\tNA\tNA\tNA\t\"all\"\t\"V:/MitoSys_data/Data/\"\n' % (path[i], resfile[i]))
    
fid.close()
'''
